from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
import numpy as np
from keras.preprocessing import image
from keras.models import load_model
from firebase import firebase
import datetime
import json
from picamera import PiCamera
from time import sleep
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library

camera = PiCamera()


firebase=firebase.FirebaseApplication('https://fir-test-2517c.firebaseio.com/',None)
days = ('monday', 'tuesday', 'wenesday', 'thursday','friday','saturday','sunday')


def button_take_photo():
    camera.start_preview()
    sleep(10)
    camera.capture('/home/pi/Desktop/foto.jpg')
    camera.stop_preview()
    print("Photo has been taken")


for day in days:
    
    correct_medicine="false"
    
    while correct_medicine == "false":
         
        #get para el dia actual
        jsonDataDay=firebase.get("/User","3xj66Dv9Svf2zgZ5Gqe2QnpvTXO2/hardware details/Medication/"+day)

        #cogemos el nombre del medicamento para cada dia como string
        medicine_name=json.dumps(jsonDataDay["name"])
        
        #quitamos las comillas para que quede el string solo con las letras
        medicine_day_name=medicine_name.replace('"', '')
        
              
        if (medicine_day_name!=''):
            
            print("Today is",day, "and you should take", medicine_day_name, "medicine")
            
            #***********************HACER FOTO, HACER PREDICCION Y SEGUN PREDICCION ENCENDER LED******************
            
            print("Press button to take the photo")
            GPIO.setwarnings(False) # Ignore warning for now
            GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
            GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)

            GPIO.remove_event_detect(10)
            GPIO.wait_for_edge(10,GPIO.RISING)
            button_take_photo()
            
            #Con la foto, le hago el test y printo el resultado
          
            test_image = image.load_img(('/home/pi/Desktop/foto.jpg'),target_size = (64,64))
            test_image = image.img_to_array(test_image)
            test_image = np.expand_dims(test_image, axis=0)
            classifier = load_model('/home/pi/Desktop/theModel.h5')
            result = classifier.predict(test_image)
            #training_set.class_indices
            if result[0][0] >= 0.5:
                prediction = 'metaspirina'
                print ("Metaspirina medicine has been detected")
            else:
                prediction = 'metadol'
                print ("Metadol medicine has been detected")
     
            #Encender led
            if medicine_day_name == prediction:
                print("Correct medication has been taken")
                #GREEN LED 
                GPIO.setwarnings(False) # Ignore warning for now
                GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
                GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW) # Set pin 8 to be an output pin and set initial value to low (off)
                GPIO.output(8, GPIO.HIGH) # Turn on
                sleep(15) # Sleep for 1 second
                GPIO.cleanup() # Clean up
                result = firebase.put("/User","3xj66Dv9Svf2zgZ5Gqe2QnpvTXO2/hardware details/Medication/"+day+"/taken","Taked")
                #cogemos la hora del sistema
                current_time=datetime.datetime.now()
                #nos quedamos solo con horas y minutos
                current_h_m=(str(current_time.time())[0:5])
                #lo enviamos a firebase
                result2 = firebase.put("/User","3xj66Dv9Svf2zgZ5Gqe2QnpvTXO2/hardware details/Medication/"+day+"/time_taken",current_h_m)
                correct_medicine="true"
                
            else:
                correct_medicine="false"
                print("Incorrect medication, NOT take this medicine !!")
                print("Please, try with another medicine and take a new photo")
                #RED LED
                GPIO.setwarnings(False) # Ignore warning for now
                GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
                GPIO.setup(7, GPIO.OUT, initial=GPIO.LOW) # Set pin 8 to be an output pin and set initial value to low (off)
                GPIO.output(7, GPIO.HIGH) # Turn on
                sleep(15) # Sleep for 1 second
                GPIO.cleanup() # Clean up
                
        else:
             
            print("Today is", day, "and you should NOT take any medicine")     
            correct_medicine="true"
       

        



